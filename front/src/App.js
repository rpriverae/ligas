import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Home } from './components/Home';
import { NotFound } from './components/NotFound';
import { CrearUsuario } from './components/CrearUsuario';
import { Login } from './components/Login';
import { CrearFisioterapia } from './components/CrearFisioterapia';
import { CrearEquipo } from './components/CrearEquipo';
import { ListaRegistros } from './components/ListaRegistros';
import { SideBar } from './components/SideBar';
import { DetalleEquipo } from './components/DetalleEquipo';

function App() {
  return (
    <div className="App">

      <BrowserRouter>
        <SideBar>
          <Routes>
            <Route exact path='/' element={<Home />} />
            <Route exact path='/home' element={<Home />} />
            <Route exact path='/registro' element={<CrearUsuario />} />
            <Route exact path='/login' element={<Login />} />
            <Route exact path='/solicitudCita' element={<CrearFisioterapia />} />
            <Route exact path='/registrarEquipo' element={<CrearEquipo />} />
            <Route exact path='/listaRegistros' element={<ListaRegistros />} />
            <Route exact path='/detalleEquipo' element={<DetalleEquipo/>}/>
            <Route exact path='*' element={<NotFound />} />
          </Routes>
        </SideBar>
        {/*<Header />
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route exact path='/home' element={<Home />} />
          <Route exact path='/registro' element={<CrearUsuario />} />
          <Route exact path='/login' element={<Login />} />
          <Route exact path='/solicitudCita' element={<CrearFisioterapia />} />
          <Route exact path='/registrarEquipo' element={<CrearEquipo />} />
          <Route exact path='/listaRegistros' element={<ListaRegistros />} />
          <Route exact path='*' element={<NotFound />} />
        </Routes>
  <Footer />*/}
      </BrowserRouter>

    </div>
  );
}

export default App;
