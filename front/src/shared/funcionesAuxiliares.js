import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

export function mostrarAlerta(mensaje, icono) {
    const mySwal = withReactContent(Swal);
    mySwal.fire({
        title: mensaje,
        icon: icono
    });
}

export function saveEquipoLocalStorage(item) {
    const storedEquipos = localStorage.getItem('equipos');
    if (storedEquipos) {
        const items = JSON.parse(storedEquipos);
        items.push(item);
        localStorage.setItem('equipos', JSON.stringify(items));
    } else {
        let items = [];
        items.push(item);
        localStorage.setItem('equipos', JSON.stringify(items));
    }
}