import React from 'react';
import {
    FaTh,
    FaBars,
    FaUserAlt,
    FaRegChartBar,
    FaCommentAlt,
    FaShoppingBag,
    FaThList
} from "react-icons/fa";
import { Link } from 'react-router-dom';
import logo from '../Assets/images/Logo-CIOT-001.png';

export const SideBar = ({ children }) => {
    const menuItem = [
        {
            path: "/",
            name: "Dashboard",
            icon: <FaTh />
        },
        {
            path: "/about",
            name: "About",
            icon: <FaUserAlt />
        },
        {
            path: "/analytics",
            name: "Analytics",
            icon: <FaRegChartBar />
        },
        {
            path: "/comment",
            name: "Comment",
            icon: <FaCommentAlt />
        },
        {
            path: "/product",
            name: "Product",
            icon: <FaShoppingBag />
        },
        {
            path: "/productList",
            name: "Product List",
            icon: <FaThList />
        }
    ]
    return (
        <div className="container-fluid">
            <div className="row flex-nowrap">
                <div className="sidebar col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                    <div className="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                        <Link classNameName='d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none' to='/'><img src={logo} alt="logo" /></Link>
                        <ul className="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                            <li>
                                <Link className='nav-link' to="/registro">
                                    <i className="fa-solid fa-right-to-bracket"></i> <span className="ms-1 d-none d-sm-inline">Registrate</span>
                                </Link>
                            </li>
                            <li>
                                <Link className='nav-link' to="/registrarEquipo">
                                    <i className="fa-solid fa-people-group"></i> <span className="ms-1 d-none d-sm-inline">Registrar equipo</span>
                                </Link>
                            </li>
                            <li>
                                <Link className='nav-link' to="/listaRegistros">
                                    <i className="fs-4 bi-table"></i> <span className="ms-1 d-none d-sm-inline">Equipos registrados</span>
                                </Link>
                            </li>
                        </ul>
                        <hr />
                        <div className="dropdown pb-4">
                            <a href="#" className="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i className='fas fa-user'></i>
                                <span className="d-none d-sm-inline mx-1">Pamela Rivera Espinosa </span>
                            </a>
                            <ul className="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
                                <li><a className='dropdown-item' href="/registro"><i className="fas fa-cog fa-fw"></i> Perfil</a></li>
                                <li><hr className='dropdown-divider' /></li>
                                <li><a className='dropdown-item' href="#"><i className="fas fa-sign-out-alt fa-fw"></i> Cerrar sesión</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col py-3">
                    {children}
                </div>
            </div>
        </div>
    )
}
