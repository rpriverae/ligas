import React, { useState, useEffect } from 'react'
//import { useForm } from 'react-hook-form';

export const CrearFisioterapia = () => {

  const [teamInfo, setTeamInfo] = useState({
    ruc: '',
    equipo: '',
    deporte: '',
    categoria: '',
    liga: '',
    direccion: '',
    telefono: '',
    correo: ''
  });

  const [teamInfoErrors, setTeamInfoErrors] = useState({
    ruc: '',
    equipo: '',
    deporte: '',
    categoria: '',
    liga: '',
    direccion: '',
    telefono: '',
    correo: ''
  });

  const [representanteInfo, setRepresentanteInfo] = useState({
    nombresApellidos: '',
    identificacion: '',
    telefonoRepresentante: '',
    correoRepresentante: ''
  });

  const [representanteInfoErrors, setRepresentanteInfoErrors] = useState({
    nombresApellidos: '',
    identificacion: '',
    telefonoRepresentante: '',
    correoRepresentante: ''
  });

  const [jugadoresInfo, setJugadoresInfo] = useState([]);
  const [jugadoresInfoErrors, setJugadoresInfoErrors] = useState([]);

  const handleTeamChange = (e) => {
    const { name, value } = e.target;
    setTeamInfo({
      ...teamInfo,
      [name]: value
    });
    setTeamInfoErrors({
      ...teamInfoErrors,
      [name]: value ? '' : 'Este campo ' + name + ' es obligatorio.'
    });
  };

  const handleRepresentanteChange = (e) => {
    const { name, value } = e.target;
    setRepresentanteInfo({
      ...representanteInfo,
      [name]: value
    });
    setRepresentanteInfoErrors({
      ...representanteInfoErrors,
      [name]: value ? '' : 'Este campo es obligatorio.'
    });
  };

  useEffect(() => {
    if (jugadoresInfo.length === 0) {
      setJugadoresInfo([{
        identificacion: '',
        nombresApellidos: '',
        fechaNacimiento: '',
        genero: '',
        direccion: '',
        telefono: '',
        correo: '',
        informacionImportante: '',
        cirugias: '',
        alergias: '',
        antecedentesLesiones: '',
        antecedentesFamiliares: '',
        medicacionPreescritas: '',
        posicion: '',
        numeroCamiseta: '',
        talla: ''
      }]);
    }
  }, []);

  const handleJugadorChange = (index, e) => {
    const { name, value } = e.target; // Destructure the event to access name and value
    const updatedJugadores = [...jugadoresInfo];
    updatedJugadores[index][name] = value;
    setJugadoresInfo(updatedJugadores);

    const updatedErrors = [...jugadoresInfoErrors];
    updatedErrors[index] = {
      ...updatedErrors[index],
      [name]: value ? '' : 'Este campo es obligatorio.' // Update error state accordingly
    };
    setJugadoresInfoErrors(updatedErrors);
  };

  const handleAddJugador = () => {
    setJugadoresInfo([...jugadoresInfo, {
      identificacion: '',
      nombresApellidos: '',
      fechaNacimiento: '',
      genero: '',
      direccion: '',
      telefono: '',
      correo: '',
      informacionImportante: '',
      cirugias: '',
      alergias: '',
      antecedentesLesiones: '',
      antecedentesFamiliares: '',
      medicacionPreescritas: '',
      posicion: '',
      numeroCamiseta: '',
      talla: ''
    }]);
    setJugadoresInfoErrors([...jugadoresInfoErrors, {}]);
  };

  const handleRemoveJugador = (index) => {

    if (jugadoresInfo.length === 1) {
      return;
    }

    const updatedJugadores = [...jugadoresInfo];
    updatedJugadores.splice(index, 1);
    setJugadoresInfo(updatedJugadores);

    const updatedErrors = [...jugadoresInfoErrors];
    updatedErrors.splice(index, 1);
    setJugadoresInfoErrors(updatedErrors);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Validate all fields before submitting
    // If any field is invalid, set appropriate error message and return
    // Otherwise, proceed with form submission
  };

  return (
    <div className="container formularioRegistrarEquipo">
      <br />
      <h2>Registrar Equipo</h2>
      <br />
      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>Información del equipo</legend>
          <div className='row'>
            <div className='col'>
              <div className="form-group">
                <label>RUC:</label>
                <input type="text" className="form-control" name="ruc" value={teamInfo.ruc} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.ruc}</small>
              </div>
            </div>
            <div className='col'>
              <div className="form-group">
                <label>Nombre del equipo:</label>
                <input type="text" className="form-control" name="equipo" value={teamInfo.equipo} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.equipo}</small>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Deporte:</label>
                <input type="text" className="form-control" name="deporte" value={teamInfo.deporte} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.deporte}</small>
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label>Categoía:</label>
                <input type="text" className="form-control" name="categoria" value={teamInfo.categoria} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.categoria}</small>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Liga barrial:</label>
                <input type="text" className="form-control" name="liga" value={teamInfo.liga} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.liga}</small>
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label>Dirección:</label>
                <input type="text" className="form-control" name="direccion" value={teamInfo.direccion} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.direccion}</small>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Teléfono:</label>
                <input type="text" className="form-control" name="telefono" value={teamInfo.telefono} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.telefono}</small>
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label>Correo electrónico:</label>
                <input type="text" className="form-control" name="correo" value={teamInfo.correo} onChange={handleTeamChange} />
                <small className="text-danger">{teamInfoErrors.correo}</small>
              </div>
            </div>
          </div>
        </fieldset>
        <br />
        <fieldset>
          <legend>Información del representante</legend>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Nombres y Apellidos:</label>
                <input type="text" className="form-control" name="nombresApellidos" value={representanteInfo.nombresApellidos} onChange={handleRepresentanteChange} />
                <small className="text-danger">{representanteInfoErrors.nombresApellidos}</small>
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label>Identificación:</label>
                <input type="text" className="form-control" name="identificacion" value={representanteInfo.identificacion} onChange={handleRepresentanteChange} />
                <small className="text-danger">{representanteInfoErrors.identificacion}</small>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="form-group">
                <label>Teléfono:</label>
                <input type="text" className="form-control" name="telefonoRepresentante" value={representanteInfo.telefonoRepresentante} onChange={handleRepresentanteChange} />
                <small className="text-danger">{representanteInfoErrors.telefonoRepresentante}</small>
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label>Correo electrónico:</label>
                <input type="text" className="form-control" name="correoRepresentante" value={representanteInfo.correoRepresentante} onChange={handleRepresentanteChange} />
                <small className="text-danger">{representanteInfoErrors.correoRepresentante}</small>
              </div>
            </div>
          </div>
        </fieldset>
        <br />
        <fieldset>
          <legend>Información de los jugadores:</legend>
          {jugadoresInfo.map((jugador, index) => (
            <div key={index}>
              <h6>Jugador {index + 1}</h6>
              <div className="row">
                <div className="col-md-10">
                  <div className="row">
                    <div className="col">
                      <div className="form-group">
                        <label>Identificación:</label>
                        <input type="text" className="form-control" name="identificacion" value={jugadoresInfo[index].identificacion} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].identificacion}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Nombres y Apellidos:</label>
                        <input type="text" className="form-control" name="nombresApellidos" value={jugadoresInfo[index].nombresApellidos} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].nombresApellidos}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Fecha de nacimiento:</label>
                        <input type="text" className="form-control" name="fechaNacimiento" value={jugadoresInfo[index].fechaNacimiento} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].fechaNacimiento}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Genero:</label>
                        <input type="text" className="form-control" name="genero" value={jugadoresInfo[index].genero} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].genero}</small>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <div className="form-group">
                        <label>Dirección:</label>
                        <input type="text" className="form-control" name="direccion" value={jugadoresInfo[index].direccion} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].direccion}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Teléfono:</label>
                        <input type="text" className="form-control" name="telefono" value={jugadoresInfo[index].telefono} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].telefono}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Correo electrónico:</label>
                        <input type="text" className="form-control" name="correo" value={jugadoresInfo[index].correo} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].correo}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Información importante:</label>
                        <input type="text" className="form-control" name="informacionImportante" value={jugadoresInfo[index].informacionImportante} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].informacionImportante}</small>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <div className="form-group">
                        <label>Cirugias:</label>
                        <input type="text" className="form-control" name="cirugias" value={jugadoresInfo[index].cirugias} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].cirugias}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Alergias:</label>
                        <input type="text" className="form-control" name="alergias" value={jugadoresInfo[index].alergias} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].alergias}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Antecedentes de lesiones:</label>
                        <input type="text" className="form-control" name="antecedentesLesiones" value={jugadoresInfo[index].antecedentesLesiones} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].antecedentesLesiones}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Antecedentes familiares:</label>
                        <input type="text" className="form-control" name="antecedentesFamiliares" value={jugadoresInfo[index].antecedentesFamiliares} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].antecedentesFamiliares}</small>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <div className="form-group">
                        <label>Medicación preescritas:</label>
                        <input type="text" className="form-control" name="medicacionPreescritas" value={jugadoresInfo[index].medicacionPreescritas} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].medicacionPreescritas}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Posición:</label>
                        <input type="text" className="form-control" name="posicion" value={jugadoresInfo[index].posicion} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].posicion}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Nro. De Camiseta:</label>
                        <input type="text" className="form-control" name="numeroCamiseta" value={jugadoresInfo[index].numeroCamiseta} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].numeroCamiseta}</small>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label>Talla:</label>
                        <input type="text" className="form-control" name="talla" value={jugadoresInfo[index].talla} onChange={(e) => handleJugadorChange(index, e)} />
                        <small className="text-danger">{jugadoresInfoErrors[index] && jugadoresInfoErrors[index].talla}</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-2">
                  <button type="button" className="btn btn-danger" onClick={() => handleRemoveJugador(index)} disabled={jugadoresInfo.length === 1}>Eliminar Jugador</button>
                </div>
              </div>
              <br />
            </div>
          ))}
          <button type="button" className="btn btn-primary" onClick={handleAddJugador}>Agregar Jugador</button>
        </fieldset>
        <button type="submit" className="btn btn-success mt-3">Enviar</button>
      </form>
    </div>
  )
}
