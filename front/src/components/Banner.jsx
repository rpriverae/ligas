import React from 'react'
import img_banner_1 from '../Assets/images/banner1.jpg';
import img_banner_2 from '../Assets/images/banner2.jpg';
import img_banner_3 from '../Assets/images/banner3.jpg';

export const Banner = () => {
    return (
        <div className='container' style={{ marginTop: 20, marginBottom: 20 }}>
            <div id="carouselExample" className='carousel slide'>
                <div className='carousel-inner'>
                    <div className='carousel-item active'>
                        <img src={img_banner_1} className='d-block w-100' alt="..."/>
                    </div>
                    <div className='carousel-item'>
                        <img src={img_banner_2} className='d-block w-100' alt="..."/>
                    </div>
                    <div className='carousel-item'>
                        <img src={img_banner_3} className='d-block w-100' alt="..."/>
                    </div>
                </div>
                <button className='carousel-control-prev' type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                    <span className='carousel-control-prev-icon' aria-hidden="true"></span>
                    <span className='visually-hidden'>Siguiente</span>
                </button>
                <button className='carousel-control-next' type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                    <span className='carousel-control-next-icon' aria-hidden="true"></span>
                    <span className='visually-hidden'>Anterior</span>
                </button>
            </div>
        </div>
    )
}
