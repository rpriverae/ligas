import React from 'react'
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom"
import { mostrarAlerta } from '../shared/funcionesAuxiliares';

export const Login = () => {

    const navigate = useNavigate()
    const handleLogin = () => {
        mostrarAlerta('Login exitoso', 'success');
        navigate('/');
    }

    return (
        <div className='container'>
            <section className='vh-100'>
                <div class="container h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div className='class="col-lg-12 col-xl-11"'>
                            <div className='card text-black'>
                                <div className='card-body p-md-5'>
                                    <div className='row justify-content-center'>
                                        <div class="col-md-9 col-lg-6 col-xl-5">
                                            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                                                class="img-fluid" alt="Sample image" />
                                        </div>
                                        <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                                            <form>
                                                <div class="form-outline mb-4">
                                                    <input type="email" id="form3Example3" class="form-control form-control-lg"
                                                        placeholder="Ingrese un correo electrónico valido" />
                                                </div>
                                                <div class="form-outline mb-3">
                                                    <input type="password" id="form3Example4" class="form-control form-control-lg"
                                                        placeholder="Ingrese la clave" />
                                                </div>

                                                <div class="text-center text-lg-start mt-4 pt-2">
                                                    <button type="button" class="btn btn-primary boton btn-lg" onClick={() => handleLogin()}>Ingresar</button>
                                                    <p class="small fw-bold mt-2 pt-1 mb-0">No tiene una cuenta? <Link to="/registro"
                                                        class="link-danger">Registrarse</Link></p>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}
