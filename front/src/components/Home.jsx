import React from 'react'
import { Link } from 'react-router-dom';
import img_card_1 from '../Assets/images/card1.jpg';
import img_card_2 from '../Assets/images/card2.jpg';
import img_card_3 from '../Assets/images/card3.jpg';
import { Banner } from './Banner';

export const Home = () => {
    return (
        <>
            <Banner />
            <div className='container home'>
                <div className='row row-cols-1 row-cols-md-3 g-4'>
                    <div className='col'>

                        <div className='card'>
                            <Link to='/registro'><img src={img_card_1} className='card-img-top' alt="..." /></Link>
                            <div className='card-body'>
                                <h5 className='card-title'>Crea tú usuario para acceder al beneficio</h5>
                                <p className='card-text'>Registrate en el sitio y así podras acceder a todos nuestros servicios y beneficios.</p>
                            </div>
                        </div>


                    </div>
                    <div className='col'>
                        
                            <div className='card'>
                            <Link to='/registrarEquipo'><img src={img_card_2} className='card-img-top' alt="..." /></Link>
                                <div className='card-body'>
                                    <h5 className='card-title'>Traumatológica Deportiva</h5>
                                    <p className='card-text'>Accede al sistema de gestión de tú equipo.</p>
                                </div>
                            </div>
                        
                    </div>
                    <div className='col'>
                        <div className='card'>
                            <img src={img_card_3} className='card-img-top' alt="..." />
                            <div className='card-body'>
                                <h5 className='card-title'>Card title</h5>
                                <p className='card-text'>This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
