import React, { useEffect, useState } from 'react'

export const DetalleEquipo = () => {

    const [equipo, setEquipo] = useState({});

    useEffect(() => {
        const storedRegistros = localStorage.getItem('equipos');
        if (storedRegistros) {
            const items = JSON.parse(storedRegistros);
            const equipo = items[0];
            setEquipo(equipo);
        }
    }, []);
    
    return (
        <div className='container detalleEquipo'>
            <br />
            <h2>{equipo?.teamInfo?.equipo}</h2>
            <fieldset>
                <legend>Información del equipo</legend>
                <div className='row'>
                    <div className='col'>
                        <label>RUC:    </label><br />
                        <span>{equipo?.teamInfo?.ruc}</span>
                    </div>
                    <div className='col'>
                        <label>Nombre del equipo:   </label><br />
                        <span>{equipo?.teamInfo?.equipo}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <label>Deporte:  </label><br />
                        <span>{equipo?.teamInfo?.deporte}</span>
                    </div>
                    <div className="col">
                        <label>Categoría: </label><br />
                        <span>{equipo?.teamInfo?.categoria}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <label>Liga barrial: </label><br />
                        <span>{equipo?.teamInfo?.liga}</span>
                    </div>
                    <div className="col">
                        <label>Dirección: </label><br />
                        <span>{equipo?.teamInfo?.direccion}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <label>Teléfono: </label><br />
                        <span>{equipo?.teamInfo?.telefono}</span>
                    </div>
                    <div className="col">
                        <label>Correo electrónico: </label><br />
                        <span>{equipo?.teamInfo?.correo}</span>
                    </div>
                </div>
            </fieldset>
            <br />
            <fieldset>
                <legend>Información del representante</legend>
                <div className="row">
                    <div className="col">
                        <label>Nombres y Apellidos: </label><br />
                        <span>{equipo?.representanteInfo?.nombresApellidos}</span>
                    </div>
                    <div className="col">
                        <label>Identificación: </label><br />
                        <span>{equipo?.representanteInfo?.identificacion}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <label>Teléfono: </label><br />
                        <span>{equipo?.representanteInfo?.telefonoRepresentante}</span>
                    </div>
                    <div className="col">
                        <label>Correo electrónico: </label><br />
                        <span>{equipo?.representanteInfo?.correoRepresentante}</span>
                    </div>
                </div>
            </fieldset>
            <br />
            <fieldset>
                <legend>Información de los jugadores</legend>
                <div className="row mt-3">
                    <div className="col-12">
                        <div className="table-responsive">
                            <table className=" table table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Identificación</th>
                                        <th>Nombres y Apellidos</th>
                                        <th>Fecha de nacimiento</th>
                                        <th>Genero</th>
                                        <th>Dirección</th>
                                        <th>Teléfono</th>
                                        <th>Correo electrónico</th>
                                        <th>Información importante</th>
                                        <th>Cirugias</th>
                                        <th>Alergias</th>
                                        <th>Antecedentes de lesiones</th>
                                        <th>Antecedentes familiares</th>
                                        <th>Medicación preescritas</th>
                                        <th>Posición</th>
                                        <th>Nro. De Camiseta</th>
                                        <th>Talla</th>
                                    </tr>
                                </thead>
                                <tbody className='table-group-divider'>
                                    {equipo?.jugadoresInfo?.map((jugador, index) => (
                                        <tr key={index + 1}>
                                            <td>{index + 1}</td>
                                            <td>{jugador?.identificacion}</td>
                                            <td>{jugador?.nombresApellidos}</td>
                                            <td>{jugador?.fechaNacimiento}</td>
                                            <td>{jugador?.genero}</td>
                                            <td>{jugador?.direccion}</td>
                                            <td>{jugador?.telefono}</td>
                                            <td>{jugador?.correo}</td>
                                            <td>{jugador?.informacionImportante}</td>
                                            <td>{jugador?.cirugias}</td>
                                            <td>{jugador?.alergias}</td>
                                            <td>{jugador?.antecedentesLesiones}</td>
                                            <td>{jugador?.antecedentesFamiliares}</td>
                                            <td>{jugador?.medicacionPreescritas}</td>
                                            <td>{jugador?.posicion}</td>
                                            <td>{jugador?.numeroCamiseta}</td>
                                            <td>{jugador?.talla}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    )
}
