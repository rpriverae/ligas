import React from 'react'
import img_not_found from '../Assets/images/notfound.png';

export const NotFound = () => {
  return (
    <div className='container'>
        <center>
            <br/>
            <h1>PAGINA NO ENCONTRADA</h1>
            <img src={img_not_found} alt="" />
        </center>
    </div>
  )
}
