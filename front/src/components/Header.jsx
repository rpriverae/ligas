import React from 'react'
import { Link } from 'react-router-dom';
import logo from '../Assets/images/Logo-CIOT-001.png';

export const Header = () => {
    return (
        <header>
            <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
                <div className='container-fluid'>
                    <Link classNameName='navbar-brand' to='/'><img src={logo} alt="logo" /></Link>
                    <button className='navbar-toggler' type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className='navbar-toggler-icon'></span>
                    </button>
                    <div className='collapse navbar-collapse' id="navbarSupportedContent">
                        <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
                        <li className='nav-item'>
                                <Link className='nav-link' to="/registro">Registrate</Link>
                            </li>
                            <li className='nav-item'>
                                <Link className='nav-link' to="/registrarEquipo">Registrar equipo</Link>
                            </li>
                            <li className='nav-item'>
                                <Link className='nav-link' to="/listaRegistros">Registros Traumatológica Deportiva</Link>
                            </li>
                        </ul>
                        <ul className='navbar-nav ms-auto mb-2 mb-lg-0 profile-menu'>
                            <li className='nav-item dropdown'>
                                <a className='nav-link dropdown-toggle' href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span>Pamela Rivera Espinosa </span>
                                    <i className='fas fa-user'></i>
                                </a>
                                <ul className='dropdown-menu' aria-labelledby="navbarDropdown">
                                    <li><a className='dropdown-item' href="/registro"><i className="fas fa-cog fa-fw"></i> Perfil</a></li>
                                    <li><hr className='dropdown-divider'/></li>
                                    <li><a className='dropdown-item' href="#"><i className="fas fa-sign-out-alt fa-fw"></i> Cerrar sesión</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    )
}
