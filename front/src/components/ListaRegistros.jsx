import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';


export const ListaRegistros = () => {

    const [registros, setRegistro] = useState([]);

    useEffect(() => {
        const storedRegistros = localStorage.getItem('equipos');
        if (storedRegistros) {
            const items = JSON.parse(storedRegistros);
            setRegistro(items);
        }
    }, [])

    return (
        <div className='container listaRegistros'>
            <h2>Registros Traumatológica Deportiva</h2>
            <br />
            <br />
            <div className="row mt-3">
                <div className="col-12">
                    <div className="table-responsive">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>RUC</th>
                                    <th>NOMBRE EQUIPO</th>
                                    <th>DEPORTE</th>
                                    <th>CATEGORÍA</th>
                                    <th>LIGA BARRIAL</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody className='table-group-divider'>
                                {registros.map((registro, index) => (
                                    <tr key={index}>
                                        <td>{registro?.teamInfo?.ruc}</td>
                                        <td>{registro?.teamInfo?.equipo}</td>
                                        <td>{registro?.teamInfo?.deporte}</td>
                                        <td>{registro?.teamInfo?.categoria}</td>
                                        <td>{registro?.teamInfo?.liga}</td>
                                        <td><Link to='/detalleEquipo'><i className='fa-solid fa-eye'></i></Link></td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}
