import React, { useEffect, useState } from 'react'
import { useForm, useFieldArray } from 'react-hook-form';
import { useNavigate } from "react-router-dom"
import { mostrarAlerta, saveEquipoLocalStorage } from '../shared/funcionesAuxiliares';

export const CrearEquipo = () => {

    const navigate = useNavigate()

    const { register, handleSubmit, control, formState: { errors } } = useForm({
        defaultValues: {
            jugadoresInfo: [{
                identificacion: '',
                nombresApellidos: '',
                fechaNacimiento: '',
                genero: '',
                direccion: '',
                telefono: '',
                correo: '',
                informacionImportante: '',
                cirugias: '',
                alergias: '',
                antecedentesLesiones: '',
                antecedentesFamiliares: '',
                medicacionPreescritas: '',
                posicion: '',
                numeroCamiseta: '',
                talla: ''
            }]
        }
    });

    const [categoriaOptions, setCategoriaOptions] = useState([]);

    const [generoOptions, setGeneroOptions] = useState([]);

    const [tallaOptions, setTallaOptions] = useState([]);

    useEffect(() => {
        setCategoriaOptions(['Infantil', 'Juvenil', 'Senior', 'Master']);
        setGeneroOptions(['Masculino', 'Femenino', 'No definido']);
        setTallaOptions(['XS', 'S', 'M', 'L', 'XL', 'XXL']);
    }, []);

    const { fields, append, remove } = useFieldArray({
        control,
        name: 'jugadoresInfo'
    });


    const onSubmit = (data) => {
        console.log(data);
        saveEquipoLocalStorage(data);
        mostrarAlerta('Registro exitoso', 'success');
        navigate('/listaRegistros');
    };

    const handleRemovePlayer = (index) => {
        if (fields.length === 1) {
            return;
        }
        remove(index);
    };
    return (
        <div className="container formularioRegistrarEquipo">
            <br />
            <h2>Registrar Equipo</h2>
            <br />
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <legend>Información del equipo</legend>
                    <div className='row'>
                        <div className='col'>
                            <div className="form-group">
                                <label>RUC:</label>
                                <input type="text" className="form-control" {...register('teamInfo.ruc', { required: true })} />
                                {errors.teamInfo?.ruc && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                        <div className='col'>
                            <div className="form-group">
                                <label>Nombre del equipo:</label>
                                <input type="text" className="form-control" {...register('teamInfo.equipo', { required: true })} />
                                {errors.teamInfo?.equipo && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Deporte:</label>
                                <input type="text" className="form-control" {...register('teamInfo.deporte', { required: true })} />
                                {errors.teamInfo?.deporte && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Categoría:</label>
                                <select className="form-control" {...register('teamInfo.categoria', { required: true })}>
                                    <option value="">Seleccionar categoría</option>
                                    {categoriaOptions.map(option => (
                                        <option key={option} value={option}>{option}</option>
                                    ))}
                                </select>
                                {errors.teamInfo?.categoria && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Liga barrial:</label>
                                <input type="text" className="form-control" {...register('teamInfo.liga', { required: true })} />
                                {errors.teamInfo?.liga && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Dirección:</label>
                                <input type="text" className="form-control" {...register('teamInfo.direccion', { required: true })} />
                                {errors.teamInfo?.direccion && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Teléfono:</label>
                                <input type="text" className="form-control" {...register('teamInfo.telefono', { required: true })} />
                                {errors.teamInfo?.telefono && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Correo electrónico:</label>
                                <input type="text" className="form-control" {...register('teamInfo.correo', { required: true, pattern: /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})+$/i })} />
                                {errors.teamInfo?.correo?.type === 'required' && <span className="text-danger">Este campo es obligatorio.</span>}
                                {errors.teamInfo?.correo?.type === 'pattern' && <span className="text-danger">Email invalido.</span>}
                            </div>
                        </div>
                    </div>
                </fieldset>
                <br />
                <fieldset>
                    <legend>Información del representante</legend>
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Nombres y Apellidos:</label>
                                <input type="text" className="form-control" {...register('representanteInfo.nombresApellidos', { required: true })} />
                                {errors.representanteInfo?.nombresApellidos && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Identificación:</label>
                                <input type="text" className="form-control" {...register('representanteInfo.identificacion', { required: true })} />
                                {errors.representanteInfo?.identificacion && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Teléfono:</label>
                                <input type="text" className="form-control" {...register('representanteInfo.telefonoRepresentante', { required: true })} />
                                {errors.representanteInfo?.telefonoRepresentante && <span className="text-danger">Este campo es obligatorio.</span>}
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-group">
                                <label>Correo electrónico:</label>
                                <input type="text" className="form-control" {...register('representanteInfo.correoRepresentante', { required: true, pattern: /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})+$/i })} />
                                {errors.representanteInfo?.correoRepresentante?.type === 'required' && <span className="text-danger">Este campo es obligatorio.</span>}
                                {errors.representanteInfo?.correoRepresentante?.type === 'pattern' && <span className="text-danger">Email invalido.</span>}
                            </div>
                        </div>
                    </div>
                </fieldset>
                <br />
                <fieldset>
                    <legend>Información de los jugadores: <button type="button" className="btn boton btn-primary" onClick={() => append({})}><i className='fa-solid fa-add'></i></button> </legend>
                    {fields.map((jugador, index) => (
                        <div key={jugador.id}>
                            <div className="row">
                                <div className="col-md-10">
                                    <h4 className='titulo-jugador'>Jugador {index + 1}</h4>
                                </div>
                                <div className="col-md-2">
                                    <button type="button" className="btn btn-eliminar-jugador btn-danger" onClick={() => handleRemovePlayer(index)}><i className='fa-solid fa-trash'></i></button>
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col">
                                    <div className="form-group">
                                        <label>Identificación:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.identificacion`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.identificacion && <span className="text-danger">Este campo es obligatorio.</span>}

                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Nombres y Apellidos:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.nombresApellidos`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.nombresApellidos && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Fecha de nacimiento:</label>
                                        <input type="date" className="form-control" {...register(`jugadoresInfo.${index}.fechaNacimiento`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.fechaNacimiento && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Genero:</label>
                                        <select className="form-control" {...register(`jugadoresInfo.${index}.genero`, { required: true })}>
                                            <option value="">Seleccionar genero</option>
                                            {generoOptions.map(option => (
                                                <option key={option} value={option}>{option}</option>
                                            ))}
                                        </select>
                                        {errors.jugadoresInfo?.[index]?.genero && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-group">
                                        <label>Dirección:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.direccion`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.direccion && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Teléfono:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.telefono`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.telefono && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Correo electrónico:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.correo`, { required: true, pattern: /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})+$/i })} />
                                        {errors.jugadoresInfo?.[index]?.correo?.type === 'required' && <span className="text-danger">Este campo es obligatorio.</span>}
                                        {errors.jugadoresInfo?.[index]?.correo?.type === 'pattern' && <span className="text-danger">Email invalido.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Información importante:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.informacionImportante`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.informacionImportante && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-group">
                                        <label>Cirugias:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.cirugias`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.cirugias && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Alergias:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.alergias`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.alergias && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Antecedentes de lesiones:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.antecedentesLesiones`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.antecedentesLesiones && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Antecedentes familiares:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.antecedentesFamiliares`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.antecedentesFamiliares && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-group">
                                        <label>Medicación preescritas:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.medicacionPreescritas`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.medicacionPreescritas && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Posición:</label>
                                        <input type="text" className="form-control" {...register(`jugadoresInfo.${index}.posicion`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.posicion && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Nro. De Camiseta:</label>
                                        <input type="text" className="form-control" {...register(`  .${index}.numeroCamiseta`, { required: true })} />
                                        {errors.jugadoresInfo?.[index]?.numeroCamiseta && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Talla:</label>
                                        <select className="form-control" {...register(`jugadoresInfo.${index}.talla`, { required: true })}>
                                            <option value="">Seleccionar talla</option>
                                            {tallaOptions.map(option => (
                                                <option key={option} value={option}>{option}</option>
                                            ))}
                                        </select>
                                        {errors.jugadoresInfo?.[index]?.talla && <span className="text-danger">Este campo es obligatorio.</span>}
                                    </div>
                                </div>
                            </div>
                            <br />
                        </div>
                    ))}
                </fieldset>
                <br />
                <button type="submit" className="btn btn-success boton mt-3">Enviar</button>
            </form>
        </div>
    )
}
